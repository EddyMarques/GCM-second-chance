package br.ucsal.fatorial.gcm.teste;

import org.junit.Test;

import br.ucsal.fatorial.gcm.Fatorial;
import org.junit.Assert;

public class FatorialTeste {

	 @Test
	    public void factorialZero() {
	        Fatorial f = new Fatorial();
	        int expected = 1;
	        int actual = f.fact(0);
	        Assert.assertEquals(expected, actual);
	    }
	
	 @Test
	    public void factorialDois() {
	        Fatorial f = new Fatorial();
	        int expected = 65456;
	        int actual = f.fact(2);
	        Assert.assertEquals(expected, actual);
	    }
}
